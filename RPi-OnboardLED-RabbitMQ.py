#!/usr/bin/python
#
# Raspberry Pi onboard LED control daemon
#
# Uses hardware:
#   Raspberry PI (tested model 2)
#
# Designed to control onboard status LED
# (1) Creates a localhost RabbitMQ queue
# (2) Listens for JSON formatted messages
# (3) Controls LED according to messages received
#
#   This work is licensed under a Creative Commons
#   Attribution-NonCommercial-ShareAlike 4.0
#   International License.
#
#   Copyright 2016
#   Aaron Crandall
#   acrandal@gmail.com
#

import threading
import time
from time import sleep
from Queue import *
import signal
import os
import logging
import pika
import json

exitFlag = 0

BLINK_FAST_TIMEOUT = 0.25
BLINK_SLOW_TIMEOUT = 1
BLINK_TRIPLE_TIMEOUT = BLINK_FAST_TIMEOUT
WAIT_FOREVER = None

LED_RMQ_QUEUE = "STATUS.LED"


#******************************************#
class LED_Control (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.name = "LED Control"
        self.isDone = False
        self.controlQueue = Queue()

        self.mode = "DEFAULT"  # Default
        self.timeout = WAIT_FOREVER
        self.LEDStatus = "ON"
        self.off()
        logging.info("Created" + self.name)
            #Modes -> [DEFAULT, OFF, ON, BLINK_FAST, BLINK_SLOW, BLINK_TRIPLE, BLINK_CUSTOM]

    def run(self):
        logging.info("Starting " + self.name)
        self.initialize_LED()
        while not self.isDone:
            try:
                mode = self.controlQueue.get(block=True, timeout=self.timeout)
                logging.debug("Mode message received : " + mode)
            except(Empty):
                logging.debug("Empty queue!")
            self.handle_mode(mode)
        logging.info("Exiting " + self.name)

    def stop(self):
        self.isDone = True
        self.off()

    #*** Set LED phyiscal state **#
    def initialize_LED(self):
        logging.info("Initializing LED")
        os.system("echo none >/sys/class/leds/led0/trigger")
        self.LEDStatus = "OFF"

    def flip_LED(self):
        if self.LEDStatus == "OFF":
            self.turn_LED_ON()
        else:
            self.turn_LED_OFF()

    def turn_LED_ON(self):
        logging.debug("Turning LED ON")
        os.system("echo 1 >/sys/class/leds/led0/brightness")
        self.LEDStatus = "ON"
    def turn_LED_OFF(self):
        logging.debug("Turning LED OFF")
        os.system("echo 0 >/sys/class/leds/led0/brightness")
        self.LEDStatus = "OFF"

    #*** Handling current mode as set ***#
    def handle_mode(self, mode):
        if mode == "OFF":
            self.handle_OFF_mode()
        elif mode == "ON":
            self.handle_ON_mode()
        elif mode == "BLINK_FAST":
            self.handle_BLINK_FAST_mode()
        elif mode == "BLINK_SLOW":
            self.handle_BLINK_SLOW_mode()
        elif mode == "BLINK_TRIPLE":
            self.handle_BLINK_TRIPLE_mode()
        elif mode == "BLINK_CUSTOM":
            self.handle_BLINK_CUSTOM_mode()
        else:
            logging.error("UNKNOWN LED Mode: " + mode)
    
    def handle_OFF_mode(self):
        self.turn_LED_OFF()
        self.mode = "OFF"
        self.timeout = WAIT_FOREVER
    def handle_ON_mode(self):
        self.turn_LED_ON()
        self.mode = "ON"
        self.timeout = WAIT_FOREVER
    def handle_BLINK_FAST_mode(self):
        self.flip_LED()
        self.mode = "BLINK_FAST"
        self.timeout = BLINK_FAST_TIMEOUT
    def handle_BLINK_SLOW_mode(self):
        self.flip_LED()
        self.mode = "BLINK_SLOW"
        self.timeout = BLINK_SLOW_TIMEOUT
    def handle_BLINK_TRIPLE_mode(self):
        if self.blinkCount == 3:
            self.turn_LED_OFF()
            sleep(BLINK_TRIPLE_TIMEOUT)

        if self.blinkCount > 0:
            self.turn_LED_ON()
            sleep(BLINK_TRIPLE_TIMEOUT)
            self.turn_LED_OFF()
            self.blinkCount -= 1
            self.timeout = BLINK_SLOW_TIMEOUT
        elif self.blinkCount <= 0:
            self.blinkCount = 0
            self.off()
    def handle_BLINK_CUSTOM_mode(self):
        self.flip_LED()
        self.mode = "BLINK_CUSTOm"
            # Timeout set in initial call

    #*** External Interface for setting mode ***#
    def off(self):
        self.controlQueue.put("OFF")
    def on(self):
        self.controlQueue.put("ON")
    def blink_fast(self):
        self.controlQueue.put("BLINK_FAST")
    def blink_slow(self):
        self.controlQueue.put("BLINK_SLOW")
    def blink_triple(self):
        self.blinkCount = 3
        self.controlQueue.put("BLINK_TRIPLE")
    def blink_custom(self, sleep_time):
        self.timeout = sleep_time
        self.controlQueue.put("BLINK_CUSTOM")



#******************************************#
class MessageHandler (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.ledControl = LED_Control()
        self.name = "RabbitMQ message Handler"
        self.isDone = False
        self.rabbitMQSetup()
        logging.info("Created " + self.name)
    def rabbitMQSetup(self):
        logging.debug("Setting up RabbitMQ connection to queue: " + LED_RMQ_QUEUE)
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(
                       'localhost'))
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=LED_RMQ_QUEUE)
        self.channel.basic_consume(self.handleMessage, queue=LED_RMQ_QUEUE, no_ack=True)
        logging.debug("RabbitMQ connected")

    def run(self):
        logging.info("Starting " + self.name)
        self.ledControl.start()
        self.channel.start_consuming()
        print "Exiting " + self.name
    def stop(self):
        logging.debug("Stopping " + self.name)
        self.isDone = True
        self.ledControl.stop()
        self.channel.basic_publish(exchange='', routing_key=LED_RMQ_QUEUE, body='STOP')

    def handleMessage(self, ch, method, properties, body):
        logging.debug("Handler got: " + body)
        if body == "STOP":
            self.channel.stop_consuming()
            return
        try:
            dat = json.loads(body)
            if dat['command'] == 'OFF':
                self.ledControl.off()
            elif dat['command'] == 'ON':
                self.ledControl.on()
            elif dat['command'] == 'BLINK_FAST':
                self.ledControl.blink_fast()
            elif dat['command'] == 'BLINK_SLOW':
                self.ledControl.blink_slow()
            elif dat['command'] == 'BLINK_TRIPLE':
                self.ledControl.blink_triple()
            elif dat['command'] == 'BLINK_CUSTOM':
                self.ledControl.blink_triple(int(dat['interval']))
        except:
            logging.error("Received message not valid JSON: " + body)

	


#*****************************************#
def sig_handler(signum, frame):
    logging.debug('Signal handler called with signal ' + str(signum))


#*****************************************#
def main():
    signal.signal(signal.SIGINT, sig_handler)

    logging.basicConfig(level=logging.WARN, format='%(relativeCreated)6d %(threadName)s %(message)s')
    logging.info("Starting up LED Control Daemon")

    messageHandler = MessageHandler()
    messageHandler.start()

    signal.pause()  #** Resumes on any signal

    messageHandler.stop()

    logging.info("Exiting Main Thread")


#*****************************************#
if __name__ == "__main__":
    main()



