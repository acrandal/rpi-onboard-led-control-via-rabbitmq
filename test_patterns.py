#!/usr/bin/env python
import pika
import json
import pprint

connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='STATUS.LED')

dat = {
        'command': "ON",
        'interval' : 2
      }

msg = ""
msg = json.dumps(dat)

channel.basic_publish(exchange='',
                      routing_key='STATUS.LED',
                      body=msg)
print(" [x] Sent '" + msg + "'")
connection.close()
